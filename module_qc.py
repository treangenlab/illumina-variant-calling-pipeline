"""This module is responsible for illumina paired-end sequencing data quality control"""
import os
import subprocess
import argparse
import sys

def run_trimmomatic(read_1, read_2, output_prefix, output_dir, log_dir,
                    adapter_file,
                    num_cores,
                    s_window_size, s_window_q_threshold,
                    min_len,
                    leading_q_threshold=3, trailing_q_threshold=3):
    '''trim reads with universal adapters using trimmomatic'''
    subprocess.run(["trimmomatic",
                    "PE",
                    "-quiet",
                    "-threads", str(num_cores),
                    read_1,
                    read_2,
                    "-baseout", os.path.join(output_dir, f"{output_prefix}.fastq"),
                    f"ILLUMINACLIP:{adapter_file}:2:30:10", # parameters: {seed_mismatches}:{palindrome_clip_threshold}:{simple_clip_threshold}
                    f"LEADING:{leading_q_threshold}",
                    f"TRAILING:{trailing_q_threshold}",
                    f"SLIDINGWINDOW:{s_window_size}:{s_window_q_threshold}",
                    f"MINLEN:{min_len}",
                    "-summary", os.path.join(log_dir, f"{output_prefix}.trimmomatic.log")],
                    check=True)

    subprocess.run(["mv",
                    os.path.join(output_dir, f"{output_prefix}_1P.fastq"),
                    os.path.join(output_dir, f"{output_prefix}_1.fastq")],
                    check=True)
    subprocess.run(["mv",
                    os.path.join(output_dir, f"{output_prefix}_2P.fastq"),
                    os.path.join(output_dir, f"{output_prefix}_2.fastq")],
                    check=True)

    if os.path.exists(os.path.join(output_dir, f"{output_prefix}_1U.fastq")):
        os.remove(os.path.join(output_dir, f"{output_prefix}_1U.fastq"))
    if os.path.exists(os.path.join(output_dir, f"{output_prefix}_2U.fastq")):
        os.remove(os.path.join(output_dir, f"{output_prefix}_2U.fastq"))

def run_fastp(read_1, read_2, output_prefix, output_dir, log_dir,
              adapter_file,
              num_cores,
              s_window_size, s_window_q_threshold,
              min_len,
              correction,
              base_q_threshold=15, unqualified_limit=40,
              n_base_limit=5,
              low_complexity_threshold=30,
              no_html=True):
    '''trim reads using fastp'''
    if adapter_file != "" and correction:
        subprocess.run(["fastp",
                        "--in1", read_1, "--in2", read_2,
                        "--out1", os.path.join(output_dir, f"{output_prefix}_1.fastq"),
                        "--out2", os.path.join(output_dir, f"{output_prefix}_2.fastq"),
                        "--adapter_fasta", adapter_file,
                        "--cut_front",
                        "--cut_window_size", str(s_window_size),
                        "--cut_mean_quality", str(s_window_q_threshold),
                        "--qualified_quality_phred", str(base_q_threshold),
                        "--unqualified_percent_limit", str(unqualified_limit),
                        "--n_base_limit", str(n_base_limit),
                        "--length_required", str(min_len),
                        "--low_complexity_filter", "--low_complexity_filter", str(low_complexity_threshold),
                        "--correction",
                        "--json", os.path.join(log_dir, f"{output_prefix}.fastp.log"),
                        "--html", os.path.join(log_dir, f"{output_prefix}.fastp.html"),
                        "--report_title", output_prefix,
                        "--thread", str(num_cores)],
                       check=True)
    elif correction:
        subprocess.run(["fastp",
                        "--in1", read_1, "--in2", read_2,
                        "--out1", os.path.join(output_dir, f"{output_prefix}_1.fastq"),
                        "--out2", os.path.join(output_dir, f"{output_prefix}_2.fastq"),
                        "--detect_adapter_for_pe",
                        "--cut_front",
                        "--cut_window_size", str(s_window_size),
                        "--cut_mean_quality", str(s_window_q_threshold),
                        "--qualified_quality_phred", str(base_q_threshold),
                        "--unqualified_percent_limit", str(unqualified_limit),
                        "--n_base_limit", str(n_base_limit),
                        "--length_required", str(min_len),
                        "--low_complexity_filter", "--low_complexity_filter", str(low_complexity_threshold),
                        "--correction",
                        "--json", os.path.join(log_dir, f"{output_prefix}.fastp.log"),
                        "--html", os.path.join(log_dir, f"{output_prefix}.fastp.html"),
                        "--report_title", output_prefix,
                        "--thread", str(num_cores)],
                       check=True)
    if adapter_file != "" and not correction:
        subprocess.run(["fastp",
                        "--in1", read_1, "--in2", read_2,
                        "--out1", os.path.join(output_dir, f"{output_prefix}_1.fastq"),
                        "--out2", os.path.join(output_dir, f"{output_prefix}_2.fastq"),
                        "--adapter_fasta", adapter_file,
                        "--cut_front",
                        "--cut_window_size", str(s_window_size),
                        "--cut_mean_quality", str(s_window_q_threshold),
                        "--qualified_quality_phred", str(base_q_threshold),
                        "--unqualified_percent_limit", str(unqualified_limit),
                        "--n_base_limit", str(n_base_limit),
                        "--length_required", str(min_len),
                        "--low_complexity_filter", "--low_complexity_filter", str(low_complexity_threshold),
                        "--json", os.path.join(log_dir, f"{output_prefix}.fastp.log"),
                        "--html", os.path.join(log_dir, f"{output_prefix}.fastp.html"),
                        "--report_title", output_prefix,
                        "--thread", str(num_cores)],
                       check=True)
    elif not correction:
        subprocess.run(["fastp",
                        "--in1", read_1, "--in2", read_2,
                        "--out1", os.path.join(output_dir, f"{output_prefix}_1.fastq"),
                        "--out2", os.path.join(output_dir, f"{output_prefix}_2.fastq"),
                        "--detect_adapter_for_pe",
                        "--cut_front",
                        "--cut_window_size", str(s_window_size),
                        "--cut_mean_quality", str(s_window_q_threshold),
                        "--qualified_quality_phred", str(base_q_threshold),
                        "--unqualified_percent_limit", str(unqualified_limit),
                        "--n_base_limit", str(n_base_limit),
                        "--length_required", str(min_len),
                        "--low_complexity_filter", "--low_complexity_filter", str(low_complexity_threshold),
                        "--json", os.path.join(log_dir, f"{output_prefix}.fastp.log"),
                        "--html", os.path.join(log_dir, f"{output_prefix}.fastp.html"),
                        "--report_title", output_prefix,
                        "--thread", str(num_cores)],
                       check=True)

    if no_html:
        if os.path.exists(os.path.join(log_dir, f"{output_prefix}.fastp.html")):
            os.remove(os.path.join(log_dir, f"{output_prefix}.fastp.html"))


def main():
    '''main function'''
    parser = argparse.ArgumentParser(description="Quality Control for Illumina PE Sequencing Data")

    parser.add_argument("read1", type=str, help="read 1 input file for paired-end reads")
    parser.add_argument("read2", type=str, help="read 2 input file for paired-end reads")
    parser.add_argument("outPrefix", type=str, help="prefix of the output files")
    parser.add_argument("outDir", type=str, help="output directory")
    parser.add_argument("logDir", type=str, help="log directory")

    parser.add_argument("-trimmomatic", dest='trimmomatic', action='store_true',
                        help="Use Trimmomatic instead of Fastp for quality control, default: [False]")
    parser.add_argument("-adapters", type=str, default="",
                        help="Fasta file containing adapter sequences, required for Trimmomatic")

    # universal parameters
    parser.add_argument("-slidingWindowSize", type=int, default=4,
                        help="Sliding window size for both trimmomatic and fastp. Default: [4]")
    parser.add_argument("-slidingWindowQuality", type=int, default=15,
                        help="Sliding window quality threshold for both trimmomatic and fastp. Default: [15]")
    parser.add_argument("-minLen", type=int, default=15,
                        help="Minimum length required for a read to be valid.  Default: [15]")
    # fastp parameters
    parser.add_argument("-fastpErrorCorrection", dest='correction', action='store_true',
                        help="Enable base correction in overlapped regions using Fastp. Default: [False]")
    # parallelization parameters
    parser.add_argument("-numcore", type=int, default=1,
                        help="Number of processor used for parallelization.  Default: [1]")

    parser.set_defaults(trimmomatic=False)
    parser.set_defaults(correction=False)
    args = parser.parse_args()

    read_1 = args.read1
    read_2 = args.read2
    output_dir = args.outDir
    output_prefix = args.outPrefix
    log_dir = args.logDir

    trimmomatic = args.trimmomatic
    adapter_file = args.adapters

    s_window_size = args.slidingWindowSize
    s_window_q_threshold = args.slidingWindowQuality
    min_len = args.minLen
    correction = args.correction
    num_cores = args.numcore
    
    if trimmomatic:
        if adapter_file == "":
            print("Adapter sequences missing.")
            sys.exit(1)
        else:
            if os.path.exists(adapter_file):
                print("Adapter removal and read trimming with Trimmomatic.")
                run_trimmomatic(read_1, read_2, output_prefix, output_dir, log_dir, adapter_file, num_cores, s_window_size, s_window_q_threshold, min_len)
            else:
                print("Adapter file not found.")
                sys.exit(1)
    else:
        print("Adapter removal and read trimming with Fastp.")
        if adapter_file == "":
            print("No adapter sequence found, enable adapter auto-detection.")
        else:
            if not os.path.exists(adapter_file):
                print("Adapter file not found.")
                sys.exit(1)
        if correction:
            print("Error correction enabled")
        run_fastp(read_1, read_2, output_prefix, output_dir, log_dir, adapter_file, num_cores, s_window_size, s_window_q_threshold, min_len, correction)

if __name__ == "__main__":
    main()