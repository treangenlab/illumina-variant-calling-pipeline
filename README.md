# illumina variant calling pipeline

usage: illumina_vcp.py [-h] [-numcore NUMCORE] [-adapters ADAPTERS]
                       [--no-dedup] [--no-indel]
                       reference metadata output

## Required inputs:

- reference: reference genome should be in fasta format.
- metadata: a text file containing a list of SRA accession number, sperated by newline.
- output: output directory

## Optional inputs:

- -numcore    for multi-core processing, default:1
- -adapters   Adapters in fasta format, by default, the program would use a universal set of adapters in adapters.fa
- --no-dedup  Do not activate deduplication step
- --no-indel  Do not call indels, only call iSNV

## Dependencies:

- SRA Toolkit (2.8.2)
- Trimmomatic (0.39)
- BWA-MEM (0.7.17-r1188)
- Samtools (1.11) 
- Lofreq (2.1.4)

